package com.frl.upw.bigdata;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import lombok.extern.slf4j.Slf4j;

/**
 * kafka producer from tsv file
 * 
 * @author kml
 */
@Slf4j
public class TsvKafkaProducer {
  public static void main(String[] args) {

    Properties props = new Properties();
    props.put("bootstrap.servers", "9771c7f9c409:9092");
    props.put("acks", "all");
    props.put("retries", 0);
    props.put("batch.size", 16384);
    props.put("linger.ms", 1);
    props.put("buffer.memory", 33554432);
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

    Producer<String, String> producer = new KafkaProducer<>(props);

    BufferedReader reader = null;
    try {
      int size = 0;
      reader = Files.newBufferedReader(Paths.get("input.tsv"), StandardCharsets.ISO_8859_1);
      String line;
      while ((line = reader.readLine()) != null) {
        int partition = size % 5;
        producer.send(
            new ProducerRecord<String, String>("tsv-topic", partition, String.valueOf(size), line),
            new Callback() {
              public void onCompletion(RecordMetadata metadata, Exception e) {
                if (e != null) {
                  e.printStackTrace();
                  System.exit(2);
                }
              }

            });

        size++;
      }

      System.out.println(size);
    } catch (IOException ioexp) {
      log.error("exception reading tsv file", ioexp);
    } finally {
      producer.close();
      try {
        reader.close();
      } catch (IOException e) {
        log.error("couldn't close reader", e);
      }
    }
  }
}
